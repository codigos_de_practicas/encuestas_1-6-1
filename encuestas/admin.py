# -*- coding: utf8 -*-
from django.contrib import admin
from encuestas.models import Pregunta, Respuesta # Importamos todos los objetos de nuestra aplicación

# Register your models here.
class PreguntaAdmin(admin.ModelAdmin):
    list_display = ['pregunta', 'fecha_publicacion'] # Agregamos los campos que queramos como columnas del admin
    search_fields = ['pregunta'] # Especificamos los campos por los cuales buscará nuestro admin 
    list_filter = ['fecha_publicacion'] # Especificamos los campos que podrá filtrar nuestro admin
admin.site.register(Pregunta, PreguntaAdmin) # La clase Pregunta del models, será administrado por la clase PreguntaAdmin del admin.py

class RespuestaAdmin(admin.ModelAdmin):
    list_display = ['opcion', 'votos'] # Agregamos los campos que queramos como columnas del admin
    search_fields = ['pregunta__pregunta', 'pregunta__fecha_publicacion', 'opcion'] # Especificamos los campos por los cuales buscará nuestro admin 
    list_filter = ['pregunta__pregunta','opcion'] # Especificamos los campos que podrá filtrar nuestro admin
admin.site.register(Respuesta, RespuestaAdmin) # La clase Respuesta del models, será administrado por la clase RespuestaAdmin del admin.py

